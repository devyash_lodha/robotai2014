#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <string>
#include <opencv2/opencv.hpp>

class configuration
{
public:
    configuration();
    //etcetera
    //server name [for the communication]
    std::string serverName = "node0";
    //welcome message for application startup
    std::string welcomeMessage = "Welcome to the program!";//
    //camera field of view
    double fieldOfView = 47;//
    //max failed frames
    int maxfframes = 32;//
    //size of the frame
    cv::Size canvasSize;//
    //saves directory for screenshots
    std::string savesDir = "/home/share/save/";//

    //modules configuration
    bool trackBalls;//
    bool trackTargets;//
    bool trackOtherRobots;//
    bool calculateDistance;//
    bool showFramerate;//
    bool enableServer;//
    bool headless;//

    //camera configuration
    int captype;//
    int capdev;//
    std::string stream = "http://10.11.65.11";//
};

#endif // CONFIGURATION_H
