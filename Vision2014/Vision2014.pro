#-------------------------------------------------
#
# Project created by QtCreator 2014-03-06T15:01:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Vision2014
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hsvvars.cpp \
    configuration.cpp \
    serverstorage.cpp \
    functionstoolkit.cpp

HEADERS  += mainwindow.h \
    hsvvars.h \
    configuration.h \
    serverstorage.h \
    functionstoolkit.h

LIBS += `pkg-config --libs opencv`

FORMS    += mainwindow.ui
