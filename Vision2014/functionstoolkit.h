#ifndef FUNCTIONSTOOLKIT_H
#define FUNCTIONSTOOLKIT_H
#include <string>
#include <cstring>
#include <sstream>

class FunctionsToolkit
{
public:
    FunctionsToolkit();
    std::string toString(bool x) {
        std::stringstream str;
        str << x;
        return str.str();
    }

    std::string toString(int x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(double x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(float x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(long double x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
};

#endif // FUNCTIONSTOOLKIT_H
