#ifndef SERVERSTORAGE_H
#define SERVERSTORAGE_H
#include <opencv2/opencv.hpp>

class ServerStorage
{
public:
    ServerStorage();
    //hot goal yes or no
    bool goalHot = 0;
    //are we looking at the left or the right goal?
    bool leftGoal = 0;
    //the distance to the static goal (what we will use)
    double staticGoalDistance = 0;
    //the distance to the dynamic goal (just there to be there)
    double dynamicGoalDistance = 0;
    //the location of the dynamic goal
    cv::Point dynamicGoalLocation;
    //the location of the static goal
    cv::Point staticGoalLocation;
    //the location of the ball
    cv::Point ballLocation;
    //the nearest robot to us
    cv::Point nearestRobot;
    //the size of the dynamic goal
    cv::Size dynamicGoalSize;
    //the size of the static goal
    cv::Size staticGoalSize;
    //the size of the ball
    cv::Size ballSize;
};

#endif // SERVERSTORAGE_H
