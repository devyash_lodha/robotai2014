#include <QApplication>
#include "mainwindow.h"
#include "hsvvars.h"
#include "configuration.h"
#include "serverstorage.h"
#include "functionstoolkit.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/opencv.hpp>
#include <ctime>
#include <cmath>
#include <cstring>
#include <cfloat>
#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <sstream>
#include <vector>
#include <math.h>
#include <QtNetwork/QUdpSocket>

using namespace std;
using namespace cv;

//declare new capture object
CvCapture *cam1 = NULL;
//stores the HSV vars for the ball
HSVVars ballHSV;
//stores the HSV vars for the target
HSVVars targetHSV;
//stores the HSV vars for the robot tracking
HSVVars robotHSV;
//stores the configuration vars
configuration conf;
//stores all the processing variables
ServerStorage vars;
//toolkit of functions
FunctionsToolkit tool;
//more variables :D
const string configurationMainLocation = "/home/share/config.devx";
string configurationLocation = configurationMainLocation;
string redAllianceConfiguration = "/home/share/config-red.devx";
string blueAllianceConfiguration = "/home/share/config-blue.devx";
bool redAlliance = true;
int fframes = 0;
int saveID = 0;

void msg(String input) {
    cout << "# " << input << endl;
}

void nl(int iterations) {
    for(int i = 0; i < iterations; i++) {
        msg(" ");
    }
}

void nl() {
    nl(1);
}

void writeConfiguration() {
    if(redAlliance) {
        configurationLocation = redAllianceConfiguration;
    } else {
        configurationLocation = blueAllianceConfiguration;
    }
    //creates the onject to write the main configuration
    msg("Writing Application Variables");
    ofstream confout(configurationMainLocation.c_str());
    confout << redAllianceConfiguration;
    confout << " ";
    confout << blueAllianceConfiguration;
    confout << " ";
    confout << redAlliance;

    confout << conf.calculateDistance;
    confout << " ";
    confout << conf.enableServer;
    confout << " ";
    confout << conf.headless;
    confout << " ";
    confout << conf.showFramerate;
    confout << " ";
    confout << conf.trackBalls;
    confout << " ";
    confout << conf.trackOtherRobots;
    confout << " ";
    confout << conf.trackTargets;
    confout << " ";
    confout << conf.welcomeMessage;
    confout << " ";
    confout << conf.fieldOfView;
    confout << " ";
    confout << conf.canvasSize.width;
    confout << " ";
    confout << conf.canvasSize.height;
    confout << " ";
    confout << conf.capdev;
    confout << " ";
    confout << conf.captype;
    confout << " ";
    confout << conf.stream;
    confout << " ";
    confout << conf.serverName;
    confout << " ";
    confout << conf.savesDir;
    confout << " ";
    confout << conf.maxfframes;
    confout << " ";

    confout << targetHSV.sliderWindowName;
    confout << " ";
    confout << ballHSV.sliderWindowName;
    confout << " ";
    confout << robotHSV.sliderWindowName;
    confout << " ";
    confout.close();
    msg("Done writing the Application Configuration at\n#\t==:" + configurationMainLocation);

    //creates object to write the new configuration
    //ofstream fout(configurationLocation.c_str());
    ofstream fout("/host/share/config-red.devx");
    fout << ballHSV.hmin;
    fout << " ";
    fout << ballHSV.hmax;
    fout << " ";
    fout << ballHSV.smin;
    fout << " ";
    fout << ballHSV.smax;
    fout << " ";
    fout << ballHSV.vmin;
    fout << " ";
    fout << ballHSV.vmax;
    fout << " ";
    fout << ballHSV.accuracy;
    fout << " ";
    fout << ballHSV.blurKernel;
    fout << " ";

    fout << targetHSV.hmin;
    fout << " ";
    fout << targetHSV.hmax;
    fout << " ";
    fout << targetHSV.smin;
    fout << " ";
    fout << targetHSV.smax;
    fout << " ";
    fout << targetHSV.vmin;
    fout << " ";
    fout << targetHSV.vmax;
    fout << " ";
    fout << targetHSV.accuracy;
    fout << " ";
    fout << targetHSV.blurKernel;
    fout << " ";

    fout << robotHSV.hmin;
    fout << " ";
    fout << robotHSV.hmax;
    fout << " ";
    fout << robotHSV.smin;
    fout << " ";
    fout << robotHSV.smax;
    fout << " ";
    fout << robotHSV.vmin;
    fout << " ";
    fout << robotHSV.vmax;
    fout << " ";
    fout << robotHSV.accuracy;
    fout << " ";
    fout << robotHSV.blurKernel;
    fout << " ";

    fout.close();
    msg("Done writing the alliance configuration!");
}

void featureSelection() {
    int id = 1000;
    while(id != 0) {
        id = 1000;
        msg("=======================");
        msg("Enable/Disable modules!");
        msg("=======================");
        msg("EXIT..................0");
        msg("Calculate Distance?...1");
        msg("Socket Server.........2");
        msg("Headless?.............3");
        msg("FPS...................4");
        msg("Track Balls...........5");
        msg("Track Targets.........6");
        msg("Track Other Robots....7");
        msg("Saves Dir.............8");
        msg("Red Config............9");
        msg("Blue Config..........10");
        msg("Switch Alliance......11");
        msg("Module Settings......12");
        msg("=======================");
        msg("ID==>:");
        cin >> id;
        if(id == 0) {
            break;
        } else if(id == 1) {
            conf.calculateDistance != conf.calculateDistance;
        } else if(id == 2) {
            conf.enableServer != conf.enableServer;
        } else if(id == 3) {
            conf.headless != conf.headless;
        } else if(id == 4) {
            conf.showFramerate != conf.showFramerate;
        } else if(id == 5) {
            conf.trackBalls != conf.trackBalls;
        } else if(id == 6) {
            conf.trackTargets != conf.trackTargets;
        } else if(id == 7) {
            conf.trackOtherRobots != conf.trackOtherRobots;
        } else if(id == 8) {
            msg("Please enter the new location!\n#   Default is: /home/share/save\n#\t==>:");
            cin >> conf.savesDir;
        } else if(id == 9) {
            msg("Please enter the new location!\n#   Default is: /home/share/config-red.devx\n#\t==>:");
            cin >> redAllianceConfiguration;
        } else if(id == 10) {
            msg("Please emter the mew location!\n#   Default is: /home/share/config-blue.devx\n#\t==>:");
            cin >> blueAllianceConfiguration;
        } else if(id == 11) {
            redAlliance != redAlliance;
        }
    }
    writeConfiguration();
}

void configureModules() {
    int id = 1000;
    while(id != 0) {
        id = 1000;
        msg("=====================");
        msg("Module Configuration!");
        msg("=====================");
        msg("EXIT................0");
        msg("Ball Tracking.......1");
        msg("Target Tracking.....2");
        msg("Robot Tracking......3");
        msg("=====================");
        msg("ID==>:");
        cin >> id;
        if(id == 0) {
            break;
        } else if(id == 1) {
            msg("Ball Tracking");
            msg("Min Area\n#\t==>:");
            cin >> ballHSV.minarea;
            msg("Max area\n#\t==>:");
            cin >> ballHSV.maxarea;
        } else if(id == 2) {
            msg("Target Tracking");
            msg("Min area\n#\t==>:");
            cin >> targetHSV.minarea;
            msg("Max area\n#\t==>");
            cin >> targetHSV.maxarea;
        } else if(id == 3) {
            msg("Nothing here yet!");
        } else {
            msg("Invalid ID");
        }
    }
    writeConfiguration();
}

void configure() {
    int id = 1000;
    while(id != 0) {
        nl(8);
        msg("Entering Configuration Wizard!");
        msg("=====================");
        msg("Features to configure");
        msg("=====================");
        msg("EXIT................0");
        msg("camera..............1");
        msg("canvas..............2");
        msg("modules.............3");
        msg("features............4");
        msg("=====================");
        msg("ID==>:");
        cin >> id;
        if(id == 0) {
            writeConfiguration();
            break;
        } else if(id == 1) {
            nl();
            msg("Configuring camera!");
            bool good = 0;
            while(!good) {
                msg("Enter the captype");
                msg("\tINTERNAL....0\n#\tMJPEG.......1\n#\tAVI.........2\n#\tJPEG........3\n#\t==>:");
                cin >> conf.captype;
                if(conf.captype == 0 || conf.captype == 1 || conf.captype == 2 || conf.captype == 3) {
                    good = true;
                } else {
                    msg("Enter a valid ID!");
                }
                if(good) {
                    if(conf.captype == 0) {
                        msg("Please enter the device ID\n#\t==>:");
                        cin >> conf.capdev;
                    } else if(conf.captype == 1) {
                        msg("Please enter the URL to the stream\n#\t==>:");
                        cin >> conf.stream;
                    } else if(conf.captype == 2) {
                        msg("Please enter the URL to the AVI\n#\t==>:");
                        cin >> conf.stream;
                    } else if(conf.captype == 3) {
                        msg("Please enter the URL to the image\n#\t==>:");
                        cin >> conf.stream;
                    }
                    msg("Camera Field of View\n#\t==>:");
                    cin >> conf.fieldOfView;
                }
            }
        } else if(id == 2) {
            msg("Please enter the canvas width!\n#\t==>:");
            cin >> conf.canvasSize.width;
            msg("Please enter the canvas height!\n#\t==>:");
            cin >> conf.canvasSize.height;
        } else if(id == 3) {
            configureModules();
        } else if(id == 4) {
            featureSelection();
        } else {
            msg("Invalid ID!");
        }
        writeConfiguration();
        id = 1000;
    }
    writeConfiguration();
}

//reads the configuration files
bool readConfiguration() {
    //gathers the basic env vars
    ifstream confin(configurationMainLocation.c_str());
    //checks if the stream is open and if not, reconfigures
    if(!confin.is_open()) {
        configure();
        readConfiguration();
        return 0;
    } else {
        //read the variables
        msg("Reading the application configuration");
        confin >> redAllianceConfiguration;
        confin >> blueAllianceConfiguration;
        confin >> redAlliance;
        confin >> conf.calculateDistance;
        confin >> conf.enableServer;
        confin >> conf.headless;
        confin >> conf.showFramerate;
        confin >> conf.trackBalls;
        confin >> conf.trackOtherRobots;
        confin >> conf.trackTargets;
        confin >> conf.welcomeMessage;
        confin >> conf.fieldOfView;
        confin >> conf.canvasSize.width;
        confin >> conf.canvasSize.height;
        confin >> conf.capdev;
        confin >> conf.captype;
        confin >> conf.stream;
        confin >> conf.serverName;
        confin >> conf.savesDir;
        confin >> conf.maxfframes;
        confin >> targetHSV.sliderWindowName;
        confin >> ballHSV.sliderWindowName;
        confin >> robotHSV.sliderWindowName;
    }
    //close the file
    confin.close();
    msg("Done reading the configuration file");
    //checks which alliance we are on
    if(redAlliance) {
        configurationLocation = redAllianceConfiguration;
    } else {
        configurationLocation = blueAllianceConfiguration;
    }

    //opens the stream for the actual configuation file
    msg("Reading Alliance Configuration");
    //ifstream fin(configurationLocation.c_str());
    ifstream fin("/home/share/config-red.devx");
    if(!fin.is_open()) {
        configure();
        readConfiguration();
        return 0;
    } else {
        fin >> ballHSV.hmin;
        fin >> ballHSV.hmax;
        fin >> ballHSV.smin;
        fin >> ballHSV.smax;
        fin >> ballHSV.vmin;
        fin >> ballHSV.vmax;
        fin >> ballHSV.accuracy;
        fin >> ballHSV.blurKernel;

        fin >> targetHSV.hmin;
        fin >> targetHSV.hmax;
        fin >> targetHSV.smin;
        fin >> targetHSV.smax;
        fin >> targetHSV.vmin;
        fin >> targetHSV.vmax;
        fin >> targetHSV.accuracy;
        fin >> targetHSV.blurKernel;

        fin >> robotHSV.hmin;
        fin >> robotHSV.hmax;
        fin >> robotHSV.smin;
        fin >> robotHSV.smax;
        fin >> robotHSV.vmin;
        fin >> robotHSV.vmax;
        fin >> robotHSV.accuracy;
        fin >> robotHSV.blurKernel;
    }
    msg("Done reading the alliance configuration!");
}

void startCamera() {
    msg("Starting the camera!");
    waitKey(10000);
    if(conf.captype != 3) {
        if(conf.captype == 0) {
            cam1 = cvCaptureFromCAM(conf.capdev);
        } else if(conf.captype == 1) {
            cam1 = cvCaptureFromFile(conf.stream.data());
        } else if(conf.captype == 2) {
            cam1 = cvCaptureFromAVI(conf.stream.data());
        }
    } else {
        msg("Using Image");
    }
}

int main() {
    int k;
    readConfiguration();
    startCamera();
    writeConfiguration();
    waitKey(100);
    while(1) {
        Mat in;
        bool ok = 0;
        if(conf.captype == 3) {
            in = imread(conf.stream.data());
            if(!in.data) {
                ok = 0;
            } else {
                ok = 1;
            }
        } else {
            if(cam1) {
                in = cvQueryFrame(cam1);
                if(!in.data) {
                    ok = 0;
                } else {
                    ok = 1;
                }
            }
        }
        if(ok) {
            Mat gui;
            in.copyTo(gui);
            namedWindow("window", CV_WINDOW_AUTOSIZE);
            imshow("window", in);

            if(conf.trackTargets) {
                Mat dst, thresh, img;
                in.copyTo(img);
                img.copyTo(dst);

                if(!conf.headless) {
                    targetHSV.updateSliders();
                }
                targetHSV.updateSliders();
            }

            switch((k = waitKey(1))) {
            case 27:
                destroyAllWindows();
                return 0;
                break;

            case 'c':
                destroyAllWindows();
                configure();
                main();
                return 0;
                break;

            case 'f':
                destroyAllWindows();
                featureSelection();
                main();
                return 0;
                break;

            case 'm':
                destroyAllWindows();
                configureModules();
                main();
                return 0;
                break;

            case 'a':
                string location = conf.savesDir + "feed" + tool.toString(saveID) + ".jpg";
                imwrite(location, in);
                saveID++;
                break;
            }

        } else {
            fframes++;
            if(fframes >= conf.maxfframes) {
                msg("Error! Reconfigure!");
                configure();
            }
        }
    }
    return 0;
}
