#ifndef SERVER_H
#define SERVER_H
#include <opencv2/opencv.hpp>
#include "functionstoolkit.h"

class server
{
public:
    server();
    cv::Point3d dynamicGoalPosition;
    cv::Size dynamicGoalSize;
    cv::Point3d staticGoalPosition;
    cv::Size staticGoalSize;
    cv::Point3d ballPosition;
    cv::Size ball3d;
    bool dynamic;
    bool leftOrRight;
    void serve() {
        kit.msg(kit.toString(dynamic));
    }
private:
    FunctionsToolkit kit;
};

#endif // SERVER_H
