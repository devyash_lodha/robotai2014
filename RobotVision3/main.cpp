#include <QCoreApplication>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <cstring>
#include <cmath>
#include <ctime>
#include <time.h>
#include <sstream>
#include <fstream>
#include <ios>
#include <iostream>
#include <pthread.h>

//my headers
#include <functionstoolkit.h>
#include <hsvvars.h>
#include <server.h>

using namespace std;
using namespace cv;

Mat img;
CvCapture *cam1;
HSVVars red_ball;
HSVVars blue_ball;
HSVVars target;

string foo = "^-_-^-config beginning-^-_-^";
string confurl = "config0.devx";
bool trackTargets = 1;
bool trackBalls = 1;
bool headless = 1;
bool cRIO_udp = 1;
bool graphicServer = 0;
bool calculateDistance = 1;
bool embedded = 1;
//setup
bool redAlliance = 1;
Size canvasSize(320,240);
int captype = 0;
int capdev = 0;
string stream = "http://10.11.65.11/mjpg/video.mjpg";
double FOV = 47;

//arena
Size2f staticTargetSize(4, 32);
Size2f dynamicTargetSize(23.5, 4);
double staticTarget_distanceFromFloor = 37.5 + (staticTargetSize.height / 2);
double dynamicTarget_distanceFromFloor = 68 + (dynamicTargetSize.height / 2);
Size2f ballSize(25, 25);
Point3d camera(14, 22, 46);
int maxfframes = 64;
int fframes = 0;

void *grabImg() {
    bool good = 0;
    Mat cap;
    if(captype != 3) {
        if(cam1) {
            cap = cvQueryFrame(cam1);
        }
    } else {
        cap = imread(stream.data());
    }

    if(!cap.empty()) {
        cap.copyTo(img);
        fframes = 0;
    } else {
        fframes++;
        if(fframes > maxfframes) {
            kit.msg("FATAL: MAXIMUM FAILED FRAMES COUNT EXCEEDED!\N# I WILL NOW EXIT!");
            exit(EXIT_FAILURE);
        }
    }
}

void *findTargets() {
    if(img.empty()) {
        Mat work;
        img.copyTo(work);
        cvtColor(work, work, CV_BGR2HSV);
        inRange(work, Scalar(target.hmin, target.smin, target.vmin), Scalar(target.hmax, target.smax, target.vmax), work);
        blur(work, work, Size(target.blurKernel, target.blurKernel), Point(-1, -1), BORDER_DEFAULT);
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(work, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE, Point(0,0));
        vector<vector<Point> > contours_poly(contours.size());
        vector<Rect> boundRect(contours.size());
        for(int i = 0; i < contours.size(); i++) {
            contour_area = contourArea(contours[i]);
            if(contour_area < target.maxarea && contour_area > target.minarea) {
                approxPolyDP(contours[i], contours_poly[i], target.accuracy, true);
                if(!isContourConvex(contours_poly[i])) {
                    continue;
                }
                boundRect[i] = boundingRect(Mat(contours_poly[i]));
                if(boundRect[i].width > boundRect[i].height * 4 && boundRect[i].width < boundRect[i].height * 9) {

                }
            }
        }
    }
}

void *findBalls() {

}

void *serve() {

}

void readConfig() {
    ifstream fin(confurl.c_str(), ios::app);
    if(fin.is_open()) {
        fin >> foo;
        fin >> stream;
        fin >> trackTargets;
        fin >> trackBalls;
        fin >> headless;
        fin >> cRIO_udp;
        fin >> graphicServer;
        fin >> calculateDistance;
        fin >> embedded;
        fin >> redAlliance;
        fin >> canvasSize.width;
        fin >> canvasSize.height;
        fin >> captype;
        fin >> capdev;
        fin >> FOV;
        fin >> staticTargetSize.width;
        fin >> staticTargetSize.height;
        fin >> staticTarget_distanceFromFloor;
        fin >> dynamicTargetSize.width;
        fin >> dynamicTargetSize.height;
        fin >> dynamicTarget_distanceFromFloor;
        fin >> ballSize.width;
        fin >> ballSize.height;
        fin >> camera.x;
        fin >> camera.y;
        fin >> camera.z;
        fin >> red_ball.accuracy;
        fin >> red_ball.blurKernel;
        fin >> red_ball.hmin;
        fin >> red_ball.hmax;
        fin >> red_ball.smin;
        fin >> red_ball.smax;
        fin >> red_ball.vmin;
        fin >> red_ball.vmax;
        fin >> blue_ball.accuracy;
        fin >> blue_ball.blurKernel;
        fin >> blue_ball.hmin;
        fin >> blue_ball.hmax;
        fin >> blue_ball.smin;
        fin >> blue_ball.smax;
        fin >> blue_ball.vmin;
        fin >> blue_ball.vmax;
        fin >> target.accuracy;
        fin >> target.blurKernel;
        fin >> target.hmin;
        fin >> target.hmax;
        fin >> target.smin;
        fin >> target.smax;
        fin >> target.vmin;
        fin >> target.vmax;
        fin >> maxfframes;
    }
}

void writeConfig() {

}

bool setup() {
    readConfig();
    writeConfig();
    if(captype == 0) {
        cam1 = cvCaptureFromCAM(0);
    } else if(captype == 1) {
        cam1 = cvCaptureFromFile(stream.data());
    } else if(captype == 2) {
        cam1 = cvCaptureFromAVI(stream.data());
    } else if(captype == 3) {
        cam1 = NULL;
    } else {
        captype = 0;
        capdev = 0;
        return 0;
    }
    while(captype != 3 && !cam1) {}
    return 1;
}

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);
    setup();
    return 0;
    return a.exec();
}
