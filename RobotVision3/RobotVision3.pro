#-------------------------------------------------
#
# Project created by QtCreator 2014-03-12T19:31:57
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = RobotVision3
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    hsvvars.cpp \
    functionstoolkit.cpp

HEADERS += \
    server.h \
    hsvvars.h \
    functionstoolkit.h

LIBS += `pkg-config --libs opencv`

OTHER_FILES +=
