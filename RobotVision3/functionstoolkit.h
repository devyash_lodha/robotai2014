#ifndef FUNCTIONSTOOLKIT_H
#define FUNCTIONSTOOLKIT_H
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <cmath>

class FunctionsToolkit
{
public:
    FunctionsToolkit();
    std::string toString(bool x) {
        std::stringstream str;
        str << x;
        return str.str();
    }

    std::string toString(int x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(double x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(float x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    std::string toString(long double x) {
        std::stringstream str;
        str << x;
        return str.str();
    }
    void nl(int iterations) {
        for(int i = 0; i < iterations; i++) {
            std::cout << "\n#";
        }
    }
    void nl() {
        nl(1);
    }
    void msg(std::string string) {
        std::cout << "\n# " << string;
    }
    double rad2deg(double rads) {
        return rads * (3.14159 / 180);
    }
    double tanDeg(double input) {
        return(std::tan(rad2deg(input)));
    }
    double hypotenuse(double legA, double legB) {
        return std::sqrt((legA * legA) + (legB * legB));
    }
    double leg(double hypotenuse, double leg) {
        return std::sqrt((hypotenuse * hypotenuse) - (leg * leg));
    }
    double distance(double ROILength, double actualLength, double measuredLength, double FOV) {
        double height = (ROILength * actualLength) / measuredLength;
        return height / tanDeg(FOV / 2);
    }
};

#endif // FUNCTIONSTOOLKIT_H
