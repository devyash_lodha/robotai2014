#ifndef IMAGEFUNCTIONS_HPP
#define IMAGEFUNCTIONS_HPP

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

class ImageFunctions {
public:
    cv::Mat img, gray, hsv, grabber_tmp;
    bool img_lck, gray_lck, hsv_lck;
    void cvtColor() {
        cv::cvtColor(img, gray, CV_BGR2GRAY);
        cv::cvtColor(img, hsv, CV_BGR2HSV);
    }
    void sendTmpToActual() {
        grabber_tmp.copyTo(img);
    }
    void clearLocks() {
        img_lck = 0;
        gray_lck = 0;
        hsv_lck = 0;
    }
};

#endif // IMAGEFUNCTIONS_HPP
