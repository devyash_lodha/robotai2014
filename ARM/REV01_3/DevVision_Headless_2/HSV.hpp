#ifndef HSV_HPP
#define HSV_HPP

#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <ImageFunctions.hpp>

class HSV {
public:
    int hmin, hmax, smin, smax, vmin, vmax, kernel, epsilon, minarea, maxarea;
    void checkVariables() {
        if(hmin < hmax) {
            hmin = hmax - 1;
        }
        if(smin < smax) {
            smin = smax - 1;
        }
        if(vmin < vmax) {
            vmin = vmax - 1;
        }
        if(kernel < 1) {
            kernel = 1;
        }
        if(kernel > 128) {
            kernel = 128;
        }
        if(epsilon < 1) {
            kernel = 1;
        }
        if(epsilon > 16) {
            epsilon = 16;
        }
    }
    cv::Scalar lowerBound() {
        return cv::Scalar(hmin, smin, vmin);
    }
    cv::Scalar upperBound() {
        return cv::Scalar(hmax, smax, vmax);
    }
    cv::Size kernelSize() {
        return cv::Size(kernel, kernel);
    }
    bool checkArea(int area) {
        if(area < maxarea && area > minarea) {
            return true;
        } else {
            return false;
        }
    }
<<<<<<< HEAD
    void mkWindows(cv::Mat thresh, std::string winName) {
        cv::namedWindow(winName, CV_WINDOW_AUTOSIZE);
        std::cout << "WINDOW CREATED" << winName << std::endl;
        cv::imshow(winName, thresh);
        cv::createTrackbar("HMIN", winName, &hmin, 254, 0);
        cv::createTrackbar("HMAX", winName, &hmax, 255, 0);
        cv::createTrackbar("SMIN", winName, &smin, 254, 0);
        cv::createTrackbar("SMAX", winName, &smax, 255, 0);
        cv::createTrackbar("VMIN", winName, &vmin, 254, 0);
        cv::createTrackbar("VMAX", winName, &vmax, 255, 0);
        cv::createTrackbar("KERNEL", winName, &kernel, 128, 0);
        cv::waitKey(1);
    }
    void minMaxFix() {
        if(hmin > hmax) {
            hmin = hmax - 1;
        }
        if(smin > smax) {
            smin = smax - 1;
        }
        if(vmin > vmax) {
            vmin = vmax - 1;
        }
        if(kernel < 1) {
            kernel = 1;
        }
=======
    void mkWindows(cv::Mat thresh, std::string winTitle) {
        std::string ttl = "HSV" + winTitle;
        cv::namedWindow(ttl, CV_WINDOW_AUTOSIZE);
        if(!thresh.empty()) {
            cv::imshow(ttl, thresh);
        }
        cv::createTrackbar("HMIN", ttl, &hmin, 254, 0);
        cv::createTrackbar("HMAX", ttl, &hmax, 255, 0);
        cv::createTrackbar("SMIN", ttl, &smin, 254, 0);
        cv::createTrackbar("SMAX", ttl, &smax, 255, 0);
        cv::createTrackbar("VMIN", ttl, &vmin, 254, 0);
        cv::createTrackbar("VMAX", ttl, &vmax, 255, 0);
        cv::createTrackbar("KERNEL", ttl, &kernel, 128, 0);
        cv::createTrackbar("EPSILON", ttl, &epsilon, 16, 0);
        cv::createTrackbar("MINAREA", ttl, &minarea, 100000, 0);
        cv::createTrackbar("MAXAREA", ttl, &maxarea, 100000, 0);
>>>>>>> 6adb5c8c161b4e72c5ab3a3a1027e773e194d548
    }
};

#endif // HSV_HPP
