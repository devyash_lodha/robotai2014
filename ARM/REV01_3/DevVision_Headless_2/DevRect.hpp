#ifndef DEVRECT_HPP
#define DEVRECT_HPP

class DevRect {
public:
    double x, y, z, width, height;
    double area() {
        return width * height;
    }
    void clear() {
        x = 0;
        y = 0;
        z = 0;
        width = 0;
        height = 0;
    }
};

#endif // DEVRECT_HPP
