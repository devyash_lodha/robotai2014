#ifndef TARGETSPOTTER_HPP
#define TARGETSPOTTER_HPP
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include <DataStorage.hpp>
#include <HSV.hpp>
#include <GlobalPreferences.hpp>
#include <FunctionsToolkit.hpp>
#include <pthread.h>

using namespace cv;
using namespace std;

class TargetSpotter {
    FunctionsToolkit k1;
public:
    void processData(DataStorage vars) {
        if(vars.horizontalTargets.x != 0 && vars.verticalTargets.x !=0) {
            if(vars.verticalTargets.x > vars.horizontalTargets.x) {
                vars.leftOrRight = false;
            } else {
                vars.leftOrRight = true;
            }
        } else {
            vars.leftOrRight = NULL;
        }
    }
    
    void spot(Mat hsv, DataStorage vars, HSV params, GlobalPreferences setup) {
        Mat thresh, x;
        hsv.copyTo(x);
        inRange(x, params.lowerBound(), params.upperBound(), thresh);
        GaussianBlur(thresh, thresh, params.kernelSize(), 0.3*((params.kernel / 2) - 1) + 0.8, 0, BORDER_DEFAULT);
        threshold(thresh, thresh, 64, 255, CV_THRESH_BINARY);
        
<<<<<<< HEAD
        params.mkWindows(thresh, "TARGET");
        params.minMaxFix();
=======
        //params.mkWindows(thresh, "TARGETS");
>>>>>>> 6adb5c8c161b4e72c5ab3a3a1027e773e194d548
        
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(thresh, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE, Point(0,0));
        vector<vector<Point> > poly(contours.size());
        vector<RotatedRect> boundRect(contours.size());
        
        vars.targetReset();
        
        for(int i = 0; i < contours.size(); i++) {
            int area = contourArea(contours[i]);
            if(area > params.minarea && area < params.maxarea) {
                approxPolyDP(contours[i], poly[i], params.epsilon, true);
                if(!isContourConvex(poly[i])) {
                    continue;
                }
                boundRect[i] = minAreaRect(Mat(poly[i]));
                if(boundRect[i].size.width > boundRect[i].size.height * 4 && boundRect[i].size.width < boundRect[i].size.height * 9) {
                    vars.isGoalHot = true;
                    if(boundRect[i].size.area() > vars.horizontalTargets.area()) {
                        vars.horizontalTargets.x = boundRect[i].center.x;
                        vars.horizontalTargets.y = boundRect[i].center.y;
                        vars.horizontalTargets.z = k1.distance(setup.canvasSize.height, setup.dynamicGoal.height, boundRect[i].size.height, setup.FOV);
                        vars.horizontalTargets.width = boundRect[i].size.width;
                        vars.horizontalTargets.height = boundRect[i].size.height;
                    } else {
                        if(boundRect[i].size.height > boundRect[i].size.width * 4 && boundRect[i].size.height < boundRect[i].size.width * 9) {
                            vars.horizontalTargets.x = boundRect[i].center.x;
                            vars.horizontalTargets.y = boundRect[i].center.y;
                            vars.horizontalTargets.z = k1.distance(setup.canvasSize.height, setup.staticGoal.height, boundRect[i].size.height, setup.FOV);
                            vars.horizontalTargets.width = boundRect[i].size.width;
                            vars.horizontalTargets.height = boundRect[i].size.height;
                        } else {}
                    }
                }
                processData(vars);
            }
        }
    }
};

#endif // TARGETSPOTTER_HPP
