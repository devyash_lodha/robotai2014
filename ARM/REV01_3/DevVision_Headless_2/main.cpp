#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/flann/flann.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/opencv.hpp>

#include <libfreenect.h>
#include <libfreenect.hpp>
#include <libfreenect_sync.h>

#include <QtNetwork/QtNetwork>

#include <iostream>
#include <fstream>
#include <istream>
#include <ostream>
#include <ios>

#include <string>
#include <cstring>
#include <time.h>
#include <ctime>
#include <math.h>
#include <cmath>
#include <sstream>

#include <pthread.h>
//#include <mutex>
#include <nfs/nfs.h>

#include <HSV.hpp>
#include <ImageFunctions.hpp>
#include <DataStorage.hpp>
#include <DevRect.hpp>
#include <FunctionsToolkit.hpp>
#include <Synchronizer.hpp>
#include <TargetSpotter.hpp>
#include <GlobalPreferences.hpp>

using namespace std;
using namespace cv;

pthread_mutex_t m1 = PTHREAD_MUTEX_INITIALIZER;
//std::mutex mtx;

CvCapture *cam1;
ImageFunctions img;
HSV ball_blue;
HSV ball_red;
HSV target;
FunctionsToolkit kit;
Synchronizer s1;
DataStorage storage;
TargetSpotter targetDetector;
GlobalPreferences preference;

int MaxFailedFrames = 128;
int captureMode = 3;
int captureDevice = 0;
string captureURI = "/home/linaro/programming/repos/robotai2014/sample_img/blue_ball.jpg";
Size canvasSize(320, 240);
Size2f horizontalGoal(23.5, 4);
Size2f verticalGoal(4, 32);
double FOV = 47;

void *imageGrabber(void *tid) {
    //collect the thread ID
    long thread = (long) tid;
    pthread_mutex_lock(&m1);
    kit.msg("Grabber started in thread " + kit.toStr(thread));
    pthread_mutex_unlock(&m1);
    int failedFrames = 0;
    while(1) {
        if(captureMode == 3) {
            Mat x = imread(captureURI.data());
<<<<<<< HEAD
            if(!x.empty()) {
                pthread_mutex_lock(&m1); {
                    x.copyTo(img.img);
                    img.cvtColor();
                } pthread_mutex_unlock(&m1);
            }
=======
            pthread_mutex_lock(&m1);
            x.copyTo(img.img);
            img.cvtColor();
            pthread_mutex_unlock(&m1);
>>>>>>> 6adb5c8c161b4e72c5ab3a3a1027e773e194d548
        } else {
            if(cam1) {
                failedFrames = 0;
                Mat x = cvQueryFrame(cam1);
<<<<<<< HEAD
                pthread_mutex_lock(&m1); {
                    //img.grabImageFromCamera(cam1);
                    x.copyTo(img.img);
                    img.cvtColor();
                } pthread_mutex_unlock(&m1);
=======
                if(!x.empty()) {
                    pthread_mutex_lock(&m1);
                    x.copyTo(img.img);
                    img.cvtColor();
                    pthread_mutex_unlock(&m1);
                } else {
                    //kit.msg("I have no clue on what happened! Something wierd happened and I'm scared!");
                }
>>>>>>> 6adb5c8c161b4e72c5ab3a3a1027e773e194d548
                s1.clearFlags();
            } else {
                failedFrames++;
                kit.msg("Frame failed or corrupted");
                if(failedFrames > MaxFailedFrames) {
                    kit.msg("Capture Error! :( Exiting!");
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    pthread_exit(NULL);
}

void *findTargets(void *tid) {
    long thread = (long) tid;
    kit.msg("TargetSpotter launched in thread " + kit.toStr(thread));
    while(true) {
        if(!img.hsv.empty()) {
            Mat hsv;
            pthread_mutex_lock(&m1); {
                img.hsv.copyTo(hsv);
                targetDetector.spot(hsv, storage, target, preference);
            } pthread_mutex_unlock(&m1);
        }
    }
}

void *findBalls(void *tid) {
    long thread = (long) tid;
    kit.msg("BallSpotter launched in thread " + kit.toStr(thread));
}

int main() {
    pthread_t threads[5];
    
    if(captureMode == 0) {
        cam1 = cvCaptureFromCAM(captureDevice);
    } else if(captureMode == 1) {
        cam1 = cvCaptureFromFile(captureURI.data());
    } else if(captureMode == 2) {
        cam1 = cvCaptureFromAVI(captureURI.data());
    } else if(captureMode == 3) {
        cam1 = NULL;
    }
    
    long grabberThread = pthread_create(&threads[0], NULL, imageGrabber, (void *)0);
    kit.msg("Grabber thread ID: " + kit.toStr(grabberThread));
    long targetThread = pthread_create(&threads[1], NULL, findTargets, (void *)0);
    kit.msg("TargetTracker started in thread " + kit.toStr(targetThread));
    
    while(true) {
        if(!img.img.empty()) {
            Mat x;
            pthread_mutex_lock(&m1);
            img.hsv.copyTo(x);
            pthread_mutex_unlock(&m1);
            imshow("Win", x);
            waitKey(1);
        } else {
            //kit.msg("IMG EMPTY :(");
        }
    }
    
    pthread_exit(NULL);
    return 0;
}
