#-------------------------------------------------
#
# Project created by QtCreator 2014-03-23T21:49:24
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = VISION_ARM_HEADLESS
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.txx

LIBS += `pkg-config --libs opencv`
