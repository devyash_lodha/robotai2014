#ifndef HSV_HPP
#define HSV_HPP

#include <opencv2/highgui/highgui.hpp>
#include <ImageFunctions.hpp>

class HSV {
public:
    int hmin, hmax, smin, smax, vmin, vmax, kernel, epsilon, minarea, maxarea;
    void checkVariables() {
        if(hmin < hmax) {
            hmin = hmax - 1;
        }
        if(smin < smax) {
            smin = smax - 1;
        }
        if(vmin < vmax) {
            vmin = vmax - 1;
        }
        if(kernel < 1) {
            kernel = 1;
        }
        if(kernel > 128) {
            kernel = 128;
        }
        if(epsilon < 1) {
            kernel = 1;
        }
        if(epsilon > 16) {
            epsilon = 16;
        }
    }
    cv::Scalar lowerBound() {
        return cv::Scalar(hmin, smin, vmin);
    }
    cv::Scalar upperBound() {
        return cv::Scalar(hmax, smax, vmax);
    }
    cv::Size kernelSize() {
        return cv::Size(kernel, kernel);
    }
    bool checkArea(int area) {
        if(area < maxarea && area > minarea) {
            return true;
        } else {
            return false;
        }
    }
    void mkWindows(ImageFunctions img) {
        
    }
};

#endif // HSV_HPP
