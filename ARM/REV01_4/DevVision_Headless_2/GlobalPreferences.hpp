#ifndef GLOBALPREFERENCES_HPP
#define GLOBALPREFERENCES_HPP
#include <opencv2/opencv.hpp>

class GlobalPreferences {
public:
    GlobalPreferences();
    cv::Size2f canvasSize;
    cv::Size2f staticGoal;
    cv::Size2f dynamicGoal;
    cv::Size2f ball;
    double FOV;
    bool redAlliance;
};

#endif // GLOBALPREFERENCES_HPP
