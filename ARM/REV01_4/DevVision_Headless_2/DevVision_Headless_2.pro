#-------------------------------------------------
#
# Project created by QtCreator 2014-03-23T21:58:21
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = DevVision_Headless_2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    HSV.cpp \
    ImageFunctions.cpp \
    DataStorage.cpp \
    DevRect.cpp \
    FunctionsToolkit.cpp \
    Synchronizer.cpp \
    TargetSpotter.cpp \
    GlobalPreferences.cpp \
    BallSpotter.cpp \
    BumperSpotter.cpp

HEADERS += \
    HSV.hpp \
    ImageFunctions.hpp \
    DataStorage.hpp \
    DevRect.hpp \
    FunctionsToolkit.hpp \
    Synchronizer.hpp \
    TargetSpotter.hpp \
    GlobalPreferences.hpp \
    BallSpotter.hpp \
    BumperSpotter.hpp

LIBS +=	`pkg-config --libs opencv libfreenect`
