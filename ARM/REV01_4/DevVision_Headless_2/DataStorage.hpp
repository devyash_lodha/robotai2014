#ifndef DATASTORAGE_HPP
#define DATASTORAGE_HPP
#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <DevRect.hpp>

class DataStorage {
public:
    DevRect horizontalTargets;
    DevRect verticalTargets;
    DevRect redBall;
    DevRect blueBall;
    std::vector<DevRect> redRobots;
    std::vector<DevRect> blueRobots;
    bool isGoalHot;
    bool leftOrRight;
    void targetReset() {
        isGoalHot = false;
        leftOrRight = false;
        horizontalTargets.x = 0;
        horizontalTargets.y = 0;
        horizontalTargets.z = 0;
        horizontalTargets.width = 0;
        horizontalTargets.height = 0;
        verticalTargets.x = 0;
        verticalTargets.y = 0;
        verticalTargets.z = 0;
        verticalTargets.width = 0;
        verticalTargets.height = 0;
    }
};

#endif // DATASTORAGE_HPP