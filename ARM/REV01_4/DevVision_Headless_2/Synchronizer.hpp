#ifndef SYNCHRONIZER_HPP
#define SYNCHRONIZER_HPP

class Synchronizer {
public:
    bool targetsTracked;
    bool ballsTracked;
    bool robotsTracked;
    void clearFlags() {
        targetsTracked = 0;
        ballsTracked = 0;
        robotsTracked = 0;
    }
    void blockThreads() {
        targetsTracked = 1;
        ballsTracked = 1;
        robotsTracked = 1;
    }
};

#endif // SYNCHRONIZER_HPP
