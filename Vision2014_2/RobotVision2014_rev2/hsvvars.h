#ifndef HSVVARS_H
#define HSVVARS_H
#include <opencv2/highgui/highgui.hpp>
#include <string>

class HSVVars {
public:
    HSVVars();
    int hmin;
    int hmax;
    int smin;
    int smax;
    int vmin;
    int vmax;
    int accuracy;
    int blurKernel;
    int minarea;
    int maxarea;

    std::string sliderWindowName;
    cv::Mat img;
    cv::Mat thresh;

    void updateSliders() {
        cv::namedWindow(sliderWindowName, CV_WINDOW_FULLSCREEN);
        cv::createTrackbar("Hue Min", sliderWindowName, &hmin, 254, 0);
        cv::createTrackbar("Hue Max", sliderWindowName, &hmax, 255, 0);
        cv::createTrackbar("Sat Min", sliderWindowName, &smin, 254, 0);
        cv::createTrackbar("Sat Max", sliderWindowName, &smax, 255, 0);
        cv::createTrackbar("Val Min", sliderWindowName, &vmin, 254, 0);
        cv::createTrackbar("Val Max", sliderWindowName, &vmax, 255, 0);
        cv::createTrackbar("Acc", sliderWindowName, &accuracy, 16, 0);
        cv::createTrackbar("Blur K.", sliderWindowName, &blurKernel, 128, 0);
        cv::createTrackbar("Area Min", sliderWindowName, &minarea, 100000000, 0);
        cv::createTrackbar("Area Max", sliderWindowName, &maxarea, 100000000, 0);
        cv::resizeWindow(sliderWindowName, 240, 540);

        if(hmin > hmax) hmin = hmax - 1;
        if(smin > smax) smin = smax - 1;
        if(vmin > vmax) vmin = vmax - 1;
        if(accuracy < 1) accuracy = 1;
        if(blurKernel < 1) blurKernel = 1;
        if(minarea > maxarea) minarea = maxarea - 1;
        cv::waitKey(1);
    }

    void fixMinMax() {
        if(hmin > hmax) hmin = hmax - 1;
        if(smin > smax) smin = smax - 1;
        if(vmin > vmax) vmin = vmax - 1;
        if(accuracy < 1) accuracy = 1;
        if(blurKernel < 1) blurKernel = 1;
        if(minarea > maxarea) minarea = maxarea - 1;
    }
};

#endif // HSVVARS_H
