#-------------------------------------------------
#
# Project created by QtCreator 2014-03-11T17:22:30
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = RobotVision2014_rev2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    hsvvars.cpp \
    functionstoolkit.cpp

LIBS += `pkg-config --libs opencv`

HEADERS += \
    server.h \
    hsvvars.h \
    functionstoolkit.h
