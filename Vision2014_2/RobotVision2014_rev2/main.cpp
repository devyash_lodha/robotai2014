#include <QCoreApplication>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QtNetwork/QUdpSocket>
#include <ctime>
#include <cmath>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include <valarray>
#include <functionstoolkit.h>
#include <hsvvars.h>
#include <server.h>

using namespace cv;
using namespace std;

//features
string confurl = "configuration_main.devx";
string foo = "===END_OF_CONFIGURATION===";
bool trackTargets = 1;
bool trackBalls = 1;
bool headless = 1;
bool cRIO_udp = 1;
bool graphicServer = 0;
bool calculateDistance = 1;
bool embedded = 1;
//setup
bool redAlliance = 1;
Size canvasSize(320,240);
int captype = 0;
int capdev = 0;
string stream = "http://10.11.65.11/mjpg/video.mjpg";
double FOV = 47;
double pi = 3.141;
//arena
Size2f staticTargetSize(4, 32);
Size2f dynamicTargetSize(23.5, 4);
double staticTarget_distanceFromFloor = 37.5 + (staticTargetSize.height / 2);
double dynamicTarget_distanceFromFloor = 68 + (dynamicTargetSize.height / 2);
Size2f ballSize(25, 25);
Point3d camera(14, 22, 46);
int maxfframes = 64;
int fframes = 0;

CvCapture *cam1;
FunctionsToolkit kit;
HSVVars balls_red;
HSVVars targets_red;
HSVVars balls_blue;
HSVVars targets_blue;
HSVVars balls_hsv;
HSVVars targets_hsv;
server mainServer;
QUdpSocket *dataServer_crio;

bool writeConfiguration() {
    ofstream fout(confurl.c_str(), ios::app);
    fout << "\n\n\n\n\n";
    fout << stream;
    fout << '\t';
    fout << trackTargets;
    fout << '\t';
    fout << trackBalls;
    fout << '\t';
    fout << headless;
    fout << '\t';
    fout << cRIO_udp;
    fout << '\t';
    fout << graphicServer;
    fout << '\t';
    fout << calculateDistance;
    fout << '\t';
    fout << embedded;
    fout << '\t';
    fout << redAlliance;
    fout << '\t';
    fout << captype;
    fout << '\t';
    fout << capdev;
    fout << '\t';
    fout << FOV;
    fout << '\t';
    fout << pi;
    fout << '\t';
    fout << staticTarget_distanceFromFloor;
    fout << '\t';
    fout << dynamicTarget_distanceFromFloor;
    fout << '\t';
    fout << maxfframes;
    fout << '\t';
    fout << canvasSize.width;
    fout << '\t';
    fout << canvasSize.height;
    fout << '\t';
    fout << dynamicTargetSize.width;
    fout << '\t';
    fout << dynamicTargetSize.height;
    fout << '\t';
    fout << staticTargetSize.width;
    fout << '\t';
    fout << staticTargetSize.height;
    fout << '\t';
    fout << ballSize.width;
    fout << '\t';
    fout << ballSize.height;
    fout << '\t';
    fout << camera.x;
    fout << '\t';
    fout << camera.y;
    fout << '\t';
    fout << camera.z;
    fout << '\n';

    //hsv variables
    //blue
    fout << balls_red.accuracy;
    fout << '\t';
    fout << balls_red.blurKernel;
    fout << '\t';
    fout << balls_red.hmin;
    fout << '\t';
    fout << balls_red.smin;
    fout << '\t';
    fout << balls_red.vmin;
    fout << '\t';
    fout << balls_red.hmax;
    fout << '\t';
    fout << balls_red.smax;
    fout << '\t';
    fout << balls_red.vmax;
    fout << '\t';
    fout << targets_red.accuracy;
    fout << '\t';
    fout << targets_red.blurKernel;
    fout << '\t';
    fout << targets_red.hmin;
    fout << '\t';
    fout << targets_red.smin;
    fout << '\t';
    fout << targets_red.vmin;
    fout << '\t';
    fout << targets_red.hmax;
    fout << '\t';
    fout << targets_red.smax;
    fout << '\t';
    fout << targets_red.vmax;
    fout << '\n';
    //red
    fout << balls_blue.accuracy;
    fout << '\t';
    fout << balls_blue.blurKernel;
    fout << '\t';
    fout << balls_blue.hmin;
    fout << '\t';
    fout << balls_blue.smin;
    fout << '\t';
    fout << balls_blue.vmin;
    fout << '\t';
    fout << balls_blue.hmax;
    fout << '\t';
    fout << balls_blue.smax;
    fout << '\t';
    fout << balls_blue.vmax;
    fout << '\t';
    fout << targets_blue.accuracy;
    fout << '\t';
    fout << targets_blue.blurKernel;
    fout << '\t';
    fout << targets_blue.hmin;
    fout << '\t';
    fout << targets_blue.smin;
    fout << '\t';
    fout << targets_blue.vmin;
    fout << '\t';
    fout << targets_blue.hmax;
    fout << '\t';
    fout << targets_blue.smax;
    fout << '\t';
    fout << targets_blue.vmax;
    fout << '\t';
    fout << foo;
    fout.close();
    return 1;
}

bool readConfiguration() {
    ifstream fin(confurl.c_str(), ios::app);
    if(fin.is_open()) {
        fin >> stream;
        fin >> trackTargets;
        fin >> trackBalls;
        fin >> headless;
        fin >> cRIO_udp;
        fin >> graphicServer;
        fin >> calculateDistance;
        fin >> embedded;
        fin >> redAlliance;
        fin >> captype;
        fin >> capdev;
        fin >> FOV;
        fin >> pi;
        fin >> staticTarget_distanceFromFloor;
        fin >> dynamicTarget_distanceFromFloor;
        fin >> maxfframes;
        fin >> canvasSize.width;
        fin >> canvasSize.height;
        fin >> dynamicTargetSize.width;
        fin >> dynamicTargetSize.height;
        fin >> staticTargetSize.width;
        fin >> staticTargetSize.height;
        fin >> ballSize.width;
        fin >> ballSize.height;
        fin >> camera.x;
        fin >> camera.y;
        fin >> camera.z;

        //hsv variables
        //blue
        fin >> balls_red.accuracy;
        fin >> balls_red.blurKernel;
        fin >> balls_red.hmin;
        fin >> balls_red.smin;
        fin >> balls_red.vmin;
        fin >> balls_red.hmax;
        fin >> balls_red.smax;
        fin >> balls_red.vmax;
        fin >> targets_red.accuracy;
        fin >> targets_red.blurKernel;
        fin >> targets_red.hmin;
        fin >> targets_red.smin;
        fin >> targets_red.vmin;
        fin >> targets_red.hmax;
        fin >> targets_red.smax;
        fin >> targets_red.vmax;
        //red
        fin >> balls_blue.accuracy;
        fin >> balls_blue.blurKernel;
        fin >> balls_blue.hmin;
        fin >> balls_blue.smin;
        fin >> balls_blue.vmin;
        fin >> balls_blue.hmax;
        fin >> balls_blue.smax;
        fin >> balls_blue.vmax;
        fin >> targets_blue.accuracy;
        fin >> targets_blue.blurKernel;
        fin >> targets_blue.hmin;
        fin >> targets_blue.smin;
        fin >> targets_blue.vmin;
        fin >> targets_blue.hmax;
        fin >> targets_blue.smax;
        fin >> targets_blue.vmax;
        fin >> foo;
        fin.close();
        return 1;
    } else {
        return 0;
    }
}

int main() {
    //set up program
    kit.msg("Start");
    if(!readConfiguration) {
        kit.msg("Configuration Not Read! File Created. Use Configuration Utility!");
        writeConfiguration();
    }
    readConfiguration();

    if(captype == 0) {
        cam1 = cvCaptureFromCAM(capdev);
        waitKey(1000);
    } else if(captype == 1) {
        cam1 = cvCaptureFromFile(stream.data());
        waitKey(1000);
    } else if(captype == 2) {
        cam1 = cvCaptureFromAVI(stream.data());
        waitKey(100);
    }

    while(1) {
        bool ok = 0;
        //grab image from camera and save it in input array
        Mat input;
        if(captype == 0 || captype == 1 || captype == 2) {
            input = cvQueryFrame(cam1);
            if(input.data) {
                ok = 1;
            } else {
                ok = 0;
            }
        } else if(captype == 3) {
            input = imread(stream.data());
            if(input.data) {
                ok = 1;
            } else {
                ok = 0;
            }
        } else {
            cerr << "Illegal Capture Type! Reconfigure!";
            ok = 0;
            return -1;
        }
        //end image grabber
        //now, process the image
        if(ok) {
            if(trackTargets) {
                Mat img, dst, thresh;
                input.copyTo(img);

                if(!headless) {
                    if(redAlliance) {
                        targets_hsv.updateSliders();
                    }
                } else {
                    targets_hsv.fixMinMax();
                }

                if(redAlliance) {
                    targets_hsv.accuracy = targets_red.accuracy;
                    targets_hsv.blurKernel = targets_red.blurKernel;
                    targets_hsv.hmin = targets_red.hmin;
                    targets_hsv.smin = targets_red.smin;
                    targets_hsv.vmin = targets_red.vmin;
                    targets_hsv.hmax = targets_red.hmax;
                    targets_hsv.smax = targets_red.smax;
                    targets_hsv.vmax = targets_red.vmax;
                } else {
                    targets_hsv.accuracy = targets_blue.accuracy;
                    targets_hsv.blurKernel = targets_blue.blurKernel;
                    targets_hsv.hmin = targets_blue.hmin;
                    targets_hsv.smin = targets_blue.smin;
                    targets_hsv.vmin = targets_blue.vmin;
                    targets_hsv.hmax = targets_blue.hmax;
                    targets_hsv.smax = targets_blue.smax;
                    targets_hsv.vmax = targets_blue.vmax;
                }

                cvtColor(img, img, CV_BGR2HSV);
                inRange(img, Scalar(targets_hsv.hmin, targets_hsv.smin, targets_hsv.vmin), Scalar(targets_hsv.hmax, targets_hsv.smax, targets_hsv.vmax), img);
                blur(img, img, Size(targets_hsv.blurKernel, targets_hsv.blurKernel), Point(-1, -1), BORDER_DEFAULT);
                threshold(img, img, 128, 255, CV_THRESH_BINARY);
                img.copyTo(thresh);

                vector<vector<Point> > contours;
                vector<Vec4i> hierarchy;
                findContours(img, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE, Point(0, 0));
                vector<vector<Point> > contours_poly(contours.size());
                vector<Rect> boundRect(contours.size());
                bool dynamic = 0;
                mainServer.dynamicGoalSize.width = 0;
                mainServer.dynamicGoalSize.height = 0;
                mainServer.dynamicGoalPosition.x = 0;
                mainServer.dynamicGoalPosition.y = 0;
                mainServer.dynamicGoalPosition.z = 0;
                mainServer.staticGoalSize.width = 0;
                mainServer.staticGoalSize.height = 0;
                mainServer.staticGoalPosition.x = 0;
                mainServer.staticGoalPosition.y = 0;
                mainServer.staticGoalPosition.z = 0;
                for(int i = 0; i < contours.size(); i++) {
                    if(contourArea(contours[i]) > targets_hsv.minarea && contourArea(contours[i]) < targets_hsv.maxarea) {
                        approxPolyDP(contours[i], contours_poly[i], targets_hsv.accuracy, 1);
                        if(!isContourConvex(contours_poly[i])) {
                            continue;
                        }
                        boundRect[i] = boundingRect(Mat(contours_poly[i]));
                        if(boundRect[i].width > boundRect[i].height * 3) {
                            dynamic = 1;
                            mainServer.dynamicGoalSize.width = boundRect[i].width;
                            mainServer.dynamicGoalSize.height = boundRect[i].height;
                            mainServer.dynamicGoalPosition.x = boundRect[i].x;
                            mainServer.dynamicGoalPosition.y = boundRect[i].y;
                            mainServer.dynamicGoalPosition.z = kit.distance(canvasSize.height, dynamicTargetSize.height, boundRect[i].height, FOV);
                        }
                        if(boundRect[i].height > boundRect[i].width * 3) {
                            mainServer.staticGoalSize.width = boundRect[i].width;
                            mainServer.staticGoalSize.height = boundRect[i].height;
                            mainServer.staticGoalPosition.x = boundRect[i].x;
                            mainServer.staticGoalPosition.y = boundRect[i].y;
                            mainServer.staticGoalPosition.z = kit.distance(canvasSize.height, staticTargetSize.height, boundRect[i].height, FOV);
                        }
                    }
                }
                mainServer.dynamic = 1;
            }
        }
        mainServer.serve();
    }

    kit.nl();
    return 0;
}
